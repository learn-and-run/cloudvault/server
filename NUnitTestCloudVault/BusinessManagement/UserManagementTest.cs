﻿using NUnit.Framework;
using Moq;
using CloudVaultServer.BusinessManagement;
using CloudVaultServer.DataAccess.Repositories.Interfaces;
using CloudVaultServer.Utils.Interfaces;
using CloudVaultServer.DataAccess.EfModels;
using System.Reflection;
using System.Collections.Generic;
using CloudVaultServer.Dbo;

namespace NUnitTestCloudVault.BusinessManagement
{
    public class UserManagementTest
    {
        private UserManagement userManagement;

        [SetUp]
        public void Setup()
        {
            // Create default for constructor
            Mock<IUserRepository> userRepoMock = new Mock<IUserRepository>();
            Mock<IActivationEmailRepository> activationEmailRepoMock = new Mock<IActivationEmailRepository>();
            Mock<IValidationUserRepository> validationEmailRepoMock = new Mock<IValidationUserRepository>();
            Mock<IKnownConnectionRepository> knownConnectionRepoMock = new Mock<IKnownConnectionRepository>();
            Mock<IMailUtils> mailUtilsMock = new Mock<IMailUtils>();

            userManagement = new UserManagement(
                userRepoMock.Object,
                activationEmailRepoMock.Object,
                validationEmailRepoMock.Object,
                knownConnectionRepoMock.Object,
                mailUtilsMock.Object
            );
        }

        [Test]
        public void TestGetMsgEmailNotFind()
        {
            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            mock.Setup(userRepo => userRepo.FindByEmail(It.IsAny<string>())).Returns(default(User));

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);

            field.SetValue(this.userManagement, mock.Object);

            Assert.IsNull(userManagement.GetMsg("notValid@email.com"));
        }

        [Test]
        public void TestGetMsgCipherMessageExist()
        {
            string message = "this is the message";

            User user = new User
            {
                CipherMsg = message
            };

            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            mock.Setup(userRepo => userRepo.FindByEmail(It.IsAny<string>())).Returns(user);

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);

            field.SetValue(this.userManagement, mock.Object);

            Dictionary<string, string> dir = userManagement.GetMsg("valid@email.com");
            Assert.IsTrue(dir.ContainsKey("cipherMsg"));
        }

        [Test]
        public void TestGetMsgCipherMessageIsGood()
        {
            string message = "this is the message";

            User user = new User
            {
                CipherMsg = message
            };

            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            mock.Setup(userRepo => userRepo.FindByEmail(It.IsAny<string>())).Returns(user);

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);

            field.SetValue(this.userManagement, mock.Object);

            Dictionary<string, string> dir = userManagement.GetMsg("valid@email.com");
            Assert.IsTrue(dir.ContainsKey("cipherMsg"));
            Assert.AreEqual(message, dir["cipherMsg"]);
        }

        [Test]
        public void TestLoginEmailNotFind()
        {
            Connection connection = new Connection
            {
                Email = "notValid@email.com"
            };

            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            mock.Setup(userRepo => userRepo.FindByEmail(It.IsAny<string>())).Returns(default(User));

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);

            field.SetValue(this.userManagement, mock.Object);

            Assert.IsNull(userManagement.Login(connection));
        }

        [Test]
        public void TestLoginEmailNotActive()
        {
            Connection connection = new Connection
            {
                Email = "notActive@email.com"
            };

            User user = new User
            {
                IsActive = false
            };

            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            mock.Setup(userRepo => userRepo.FindByEmail(It.IsAny<string>())).Returns(user);

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);

            field.SetValue(this.userManagement, mock.Object);

            Assert.IsNull(userManagement.Login(connection));
        }

        [Test]
        public void TestLoginDifferentMasg()
        {
            Connection connection = new Connection
            {
                Email = "active@email.com",
                Msg = "connectionMessage"
            };

            User user = new User
            {
                IsActive = true,
                Msg = "differentMessage"
            };

            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            mock.Setup(userRepo => userRepo.FindByEmail(It.IsAny<string>())).Returns(user);

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);

            field.SetValue(this.userManagement, mock.Object);

            Assert.IsNull(userManagement.Login(connection));
        }

        [Test]
        public void TestLoginSuccess()
        {
            Connection connection = new Connection
            {
                Email = "active@email.com",
                Msg = "sameMessage"
            };

            User user = new User
            {
                IsActive = true,
                Msg = "sameMessage"
            };

            Mock<IUserRepository> userMock = new Mock<IUserRepository>();
            userMock.Setup(userRepo => userRepo.FindByEmail(It.IsAny<string>())).Returns(user);

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, userMock.Object);

            Mock<IKnownConnectionRepository> knownConnectionMock = new Mock<IKnownConnectionRepository>();
            knownConnectionMock.Setup(known => known.IsCookieUnique(It.IsAny<string>())).Returns(true);

            field = typeof(UserManagement).GetField("_knownConnectionRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, knownConnectionMock.Object);

            Assert.IsNotNull(userManagement.Login(connection));
        }

        [Test]
        public void TestCreateUserEmailNotAvailable()
        {
            Mock<IUserRepository> userMock = new Mock<IUserRepository>();
            userMock.Setup(userRepo => userRepo.IsEmailAvailable(It.IsAny<string>())).Returns(false);

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, userMock.Object);


            Assert.IsNull(userManagement.CreateUser("notAvailable@email.com"));
        }

        [Test]
        public void TestCreateUserSuccess()
        {
            Mock<IUserRepository> userMock = new Mock<IUserRepository>();
            userMock.Setup(userRepo => userRepo.IsEmailAvailable(It.IsAny<string>())).Returns(true);

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, userMock.Object);


            Mock<IActivationEmailRepository> activationMock = new Mock<IActivationEmailRepository>();
            activationMock.Setup(activation => activation.IsGuidUnique(It.IsAny<string>())).Returns(true);

            field = typeof(UserManagement).GetField("_activationEmailRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, activationMock.Object);

            Mock<IValidationUserRepository> validationMock = new Mock<IValidationUserRepository>();
            validationMock.Setup(validation => validation.IsGuidUnique(It.IsAny<string>())).Returns(true);

            field = typeof(UserManagement).GetField("_validationUserRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, validationMock.Object);

            Assert.IsNotNull(userManagement.CreateUser("valid@email.com"));
        }

        [Test]
        public void TestFinalizeUserNotFindValidationUser()
        {
            NewCipherMsg cipher = new NewCipherMsg()
            {
                Guid = "ffdsfsedéeze3RDZDSfdf2E2"
            };

            Mock<IValidationUserRepository> validationMock = new Mock<IValidationUserRepository>();
            validationMock.Setup(validation => validation.FindByGuid(It.IsAny<string>())).Returns(default(ValidationUser));

            FieldInfo field = typeof(UserManagement).GetField("_validationUserRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, validationMock.Object);


            Assert.IsFalse(userManagement.FinalizeUser(cipher));
        }

        [Test]
        public void TestFinalizeUserNotFindUserInActivationEmails()
        {
            NewCipherMsg cipher = new NewCipherMsg()
            {
                Guid = "ffdsfsedéeze3RDZDSfdf2E2",
                CipherMsg = "cipher"
            };

            Mock<IUserRepository> userMock = new Mock<IUserRepository>();
            userMock.Setup(userRepo => userRepo.FindById(It.IsAny<long>())).Returns(new User());

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, userMock.Object);

            Mock<IActivationEmailRepository> activationMock = new Mock<IActivationEmailRepository>();
            activationMock.Setup(activation => activation.FindByUserId(It.IsAny<long>())).Returns(default(ActivationEmail));

            field = typeof(UserManagement).GetField("_activationEmailRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, activationMock.Object);

            Mock<IValidationUserRepository> validationMock = new Mock<IValidationUserRepository>();
            validationMock.Setup(validation => validation.FindByGuid(It.IsAny<string>())).Returns(new ValidationUser());

            field = typeof(UserManagement).GetField("_validationUserRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, validationMock.Object);

            Assert.IsFalse(userManagement.FinalizeUser(cipher));
        }

        [Test]
        public void TestFinalizeUserNotSendMail()
        {
            NewCipherMsg cipher = new NewCipherMsg()
            {
                Guid = "ffdsfsedéeze3RDZDSfdf2E2",
                CipherMsg = "cipher"
            };

            Mock<IUserRepository> userMock = new Mock<IUserRepository>();
            userMock.Setup(userRepo => userRepo.FindById(It.IsAny<long>())).Returns(new User());

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, userMock.Object);

            Mock<IActivationEmailRepository> activationMock = new Mock<IActivationEmailRepository>();
            activationMock.Setup(activation => activation.FindByUserId(It.IsAny<long>())).Returns(new ActivationEmail());

            field = typeof(UserManagement).GetField("_activationEmailRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, activationMock.Object);

            Mock<IValidationUserRepository> validationMock = new Mock<IValidationUserRepository>();
            validationMock.Setup(validation => validation.FindByGuid(It.IsAny<string>())).Returns(new ValidationUser());

            field = typeof(UserManagement).GetField("_validationUserRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, validationMock.Object);

            Mock<IMailUtils> mailMock = new Mock<IMailUtils>();
            mailMock.Setup(mail => mail.SendMail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(false);

            field = typeof(UserManagement).GetField("_mailUtils", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, mailMock.Object);

            Assert.IsFalse(userManagement.FinalizeUser(cipher));
        }

        [Test]
        public void TestFinalizeUserSendMail()
        {
            NewCipherMsg cipher = new NewCipherMsg()
            {
                Guid = "ffdsfsedéeze3RDZDSfdf2E2",
                CipherMsg = "cipher"
            };

            Mock<IUserRepository> userMock = new Mock<IUserRepository>();
            userMock.Setup(userRepo => userRepo.FindById(It.IsAny<long>())).Returns(new User());

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, userMock.Object);

            Mock<IActivationEmailRepository> activationMock = new Mock<IActivationEmailRepository>();
            activationMock.Setup(activation => activation.FindByUserId(It.IsAny<long>())).Returns(new ActivationEmail());

            field = typeof(UserManagement).GetField("_activationEmailRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, activationMock.Object);

            Mock<IValidationUserRepository> validationMock = new Mock<IValidationUserRepository>();
            validationMock.Setup(validation => validation.FindByGuid(It.IsAny<string>())).Returns(new ValidationUser());

            field = typeof(UserManagement).GetField("_validationUserRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, validationMock.Object);

            Mock<IMailUtils> mailMock = new Mock<IMailUtils>();
            mailMock.Setup(mail => mail.SendMail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            field = typeof(UserManagement).GetField("_mailUtils", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, mailMock.Object);

            Assert.IsTrue(userManagement.FinalizeUser(cipher));
        }

        [Test]
        public void TestActivateUserNotFindActivationMail()
        {
            Mock<IActivationEmailRepository> activationMock = new Mock<IActivationEmailRepository>();
            activationMock.Setup(activation => activation.FindByGuid(It.IsAny<string>())).Returns(default(ActivationEmail));

            FieldInfo field = typeof(UserManagement).GetField("_activationEmailRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, activationMock.Object);

            Assert.IsFalse(userManagement.ActivateUser("dscfs675GS67TUW9IIJS3"));
        }

        [Test]
        public void TestActivateUserNotActive()
        {
            Mock<IUserRepository> userMock = new Mock<IUserRepository>();
            userMock.Setup(userRepo => userRepo.FindById(It.IsAny<long>())).Returns(new User() { IsActive = true, CipherMsg = "test" });

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, userMock.Object);

            Mock<IActivationEmailRepository> activationMock = new Mock<IActivationEmailRepository>();
            activationMock.Setup(activation => activation.FindByGuid(It.IsAny<string>())).Returns(new ActivationEmail());

            field = typeof(UserManagement).GetField("_activationEmailRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, activationMock.Object);

            Assert.IsFalse(userManagement.ActivateUser("dscfs675GS67TUW9IIJS3"));
        }

        [Test]
        public void TestActivateUserActiveButNotMessage()
        {
            Mock<IUserRepository> userMock = new Mock<IUserRepository>();
            userMock.Setup(userRepo => userRepo.FindById(It.IsAny<long>())).Returns(new User() { IsActive = false, CipherMsg = null });

            FieldInfo field = typeof(UserManagement).GetField("_userRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, userMock.Object);

            Mock<IActivationEmailRepository> activationMock = new Mock<IActivationEmailRepository>();
            activationMock.Setup(activation => activation.FindByGuid(It.IsAny<string>())).Returns(new ActivationEmail());

            field = typeof(UserManagement).GetField("_activationEmailRepository", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(this.userManagement, activationMock.Object);

            Assert.IsFalse(userManagement.ActivateUser("dscfs675GS67TUW9IIJS3"));
        }
    }
}
