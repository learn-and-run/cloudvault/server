﻿using CloudVaultServer.BusinessManagement.Interfaces;
using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.Dbo;
using CloudVaultServer.Utils.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CloudVaultServer.Controllers
{
    /// <summary>
    /// Class DataController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DataController : ControllerBase
    {
        private readonly ISecurityUtils _securityUtils;
        private readonly IDataManagement _dataManagement;

        /// <summary>
        /// Constructor of DataController
        /// </summary>
        /// <param name="securityUtils"></param>
        /// <param name="dataManagement"></param>
        public DataController(ISecurityUtils securityUtils,  IDataManagement dataManagement)
        {
            _securityUtils = securityUtils;
            _dataManagement = dataManagement;
        }

        /// <summary>
        /// Request to update the database
        /// </summary>
        /// <param name="updateObjectBody">Object with data to update</param>
        /// <returns>error or update the user</returns>
        [HttpPut("update")]
        public IActionResult UpdateData(UpdateObjectBody updateObjectBody)
        {
            User user = _securityUtils.GetCurrentUser(Request.Cookies["SessionId"]);
            if (user == null)
                return Unauthorized();

            return Ok(_dataManagement.UpdateUser(user.Id, updateObjectBody));
        }
    }
}