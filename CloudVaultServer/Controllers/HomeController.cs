﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using CloudVaultServer.Models;
using CloudVaultServer.BusinessManagement.Interfaces;

namespace CloudVaultServer.Controllers
{
    /// <summary>
    /// Class HomeController
    /// </summary>
    public class HomeController : Controller
    {
        private readonly IUserManagement _userManagement;

        /// <summary>
        /// Constructor HomeController
        /// </summary>
        /// <param name="userManagement"></param>
        public HomeController(IUserManagement userManagement)
        {
            _userManagement = userManagement;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Download()
        {
            return View();
        }

        public IActionResult Activate(string id)
        {
            if (!_userManagement.ActivateUser(id))
                return RedirectToAction("Error");

            return View();
        }

        public IActionResult ValidateEmail(string id)
        {
            if (!_userManagement.ValidateEmail(id))
                return RedirectToAction("Error");

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
