﻿using System.Collections.Generic;
using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.Dbo;
using Microsoft.AspNetCore.Mvc;
using CloudVaultServer.Utils.Interfaces;
using CloudVaultServer.BusinessManagement.Interfaces;

namespace CloudVaultServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    /// <summary>
    /// UserController
    /// </summary>
    public class UserController : ControllerBase
    {
        private readonly ISecurityUtils _securityUtils;
        private readonly IUserManagement _userManagement;

        /// <summary>
        /// Constructeur de UserController
        /// </summary>
        /// <param name="securityUtils"></param>
        /// <param name="userManagement"></param>
        public UserController(ISecurityUtils securityUtils, IUserManagement userManagement)
        {
            _securityUtils = securityUtils;
            _userManagement = userManagement;
        }

        /// <summary>
        /// Ask user's message with his email.  
        /// </summary>
        /// <param name="emailObject">The email of the user</param>
        /// <returns>the message which permit the authentification of the user for the server and to get user's cipherMsg back</returns>
        [HttpPost("msg")]
        public ActionResult GetMsg(EmailObject emailObject)
        {
            Dictionary<string, string> res = _userManagement.GetMsg(emailObject.Email);
            if (res == null)
                return NotFound();

            return Ok(res);
        }

        /// <summary>
        /// Request for login the user
        /// </summary>
        /// <param name="connection">The information of the user</param>
        /// <returns>200 if login else error </returns>
        [HttpPost("login")]
        public ActionResult Login(Connection connection)
        {
            string cookie = _userManagement.Login(connection);
            if (cookie == null)
                return Unauthorized();

            Response.Cookies.Append("SessionId", cookie);
            return Ok();
        }

        /// <summary>
        /// Request for logout
        /// </summary>
        /// <returns>200 if logout else error</returns>
        [HttpDelete("logout")]
        public ActionResult Logout()
        {
            _userManagement.Logout(Request.Cookies["SessionId"]);
            Response.Cookies.Delete("SessionId");
            return Ok();
        }

        /// <summary>
        /// Request for create account
        /// </summary>
        /// <param name="newUser">Information of the user for create account</param>
        /// <returns>200 if created else error</returns>
        [HttpPost("create")]
        public ActionResult CreateUser(EmailObject newUser)
        {
            NewMsg newMsg = _userManagement.CreateUser(newUser.Email);
            if (newMsg == null)
                return Conflict();

            return Ok(newMsg);
        }

        /// <summary>
        /// Request for validate the creation of the user account
        /// </summary>
        /// <param name="newCipherMsg">CipherMsg of the user</param>
        /// <returns>200 or error </returns>
        [HttpPost("finalize")]
        public ActionResult FinalizeUser(NewCipherMsg newCipherMsg)
        {
            if (!_userManagement.FinalizeUser(newCipherMsg))
                return Forbid();

            return Ok();
        }

        /// <summary>
        /// Request for update email of the user
        /// </summary>
        /// <param name="newEmail">the new email of the user</param>
        /// <returns>200 if updated else error</returns>
        [HttpPut("update_email")]
        public ActionResult UpdateEmail(EmailObject newEmail)
        {
            User user = _securityUtils.GetCurrentUser(Request.Cookies["SessionId"]);
            if (user == null)
                return Unauthorized();

            if (!_userManagement.UpdateEmail(user, newEmail.Email))
                return Conflict();

            return Ok();
        }
    }
}