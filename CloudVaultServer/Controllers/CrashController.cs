﻿using Microsoft.AspNetCore.Mvc;

namespace CloudVaultServer.Controllers
{
    /// <summary>
    /// Class CrashController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CrashController : ControllerBase
    {
        /// <summary>
        /// Endpoint to generate a crash (send info to sentry)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public int Crash()
        {
            int a = 1;
            int b = 0;
            // add a comment
            return a / b; // oups :)
        }
    }
}