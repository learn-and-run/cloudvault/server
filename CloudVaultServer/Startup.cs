using AutoMapper;
using CloudVaultServer.BusinessManagement;
using CloudVaultServer.BusinessManagement.Interfaces;
using CloudVaultServer.DataAccess.Configuration;
using CloudVaultServer.DataAccess.Repositories;
using CloudVaultServer.DataAccess.Repositories.Interfaces;
using CloudVaultServer.Utils;
using CloudVaultServer.Utils.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;

namespace CloudVaultServer
{
    public class Startup
    {
        private string _connectionString;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<CustomContext>((sp, options) =>
                    options.UseSqlServer(_connectionString).UseInternalServiceProvider(sp));

            services.AddAutoMapper(typeof(AutomapperProfiles));

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "CloudVaultAPI", Version = "v1" });
                var basePath = AppContext.BaseDirectory;
                var xmlPath = Path.Combine(basePath, "CloudVaultServer.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.AddTransient<IActivationEmailRepository, ActivationEmailRepository>();
            services.AddTransient<IDataRepository, DataRepository>();
            services.AddTransient<IKnownConnectionRepository, KnownConnectionRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IValidationUserRepository, ValidationUserRepository>();

            services.AddTransient<IUserManagement, UserManagement>();
            services.AddTransient<IDataManagement, DataManagement>();

            services.AddTransient<ISecurityUtils, SecurityUtils>();
            services.AddTransient<IMailUtils, MailUtils>();

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            _connectionString = Configuration.GetConnectionString("DbLink");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CloudVault V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
