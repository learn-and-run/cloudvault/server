﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CloudVaultServer.Dbo
{
    public class DataUpdateBody
    {
        [Required]
        public long ServerId { get; set; }
        [Required]
        public string LocalGuid { get; set; }
        [Required]
        public DateTime LastUpdate { get; set; }
        public string MetaData { get; set; }
        public string Data { get; set; }
    }
}
