﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CloudVaultServer.Dbo
{
    public class UpdateObjectBody
    {
        public DateTime? LastSync { get; set; }
        [Required]
        public List<DataUpdateBody> Datas { get; set; }
    }
}
