﻿using System.ComponentModel.DataAnnotations;

namespace CloudVaultServer.Dbo
{
    public class EmailObject
    {
        [EmailAddress]
        [Required]
        public string Email { get; set; }
    }
}
