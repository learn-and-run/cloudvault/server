﻿using System;

namespace CloudVaultServer.Dbo
{
    public class DataUpdateResponse
    {
        public long ServerId { get; set; }
        public string LocalGuid { get; set; }
        public DateTime LastUpdate { get; set; }
        public string MetaData { get; set; }
        public string Data { get; set; }
    }
}
