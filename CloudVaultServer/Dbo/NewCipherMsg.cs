﻿using System.ComponentModel.DataAnnotations;

namespace CloudVaultServer.Dbo
{
    public class NewCipherMsg
    {
        [Required]
        public string Guid { get; set; }
        [Required]
        public string CipherMsg { get; set; }
    }
}
