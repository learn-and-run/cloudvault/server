﻿using System.ComponentModel.DataAnnotations;

namespace CloudVaultServer.Dbo
{
    public class Connection
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Msg { get; set; }
    }
}
