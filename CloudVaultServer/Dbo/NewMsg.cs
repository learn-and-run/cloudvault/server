﻿using System.ComponentModel.DataAnnotations;

namespace CloudVaultServer.Dbo
{
    public class NewMsg
    {
        [Required]
        public string Guid { get; set; }
        [Required]
        public string Msg { get; set; }
    }
}
