﻿using System;
using System.Collections.Generic;

namespace CloudVaultServer.Dbo
{
    public class UpdateObjectResponse
    {
        public DateTime LastSync { get; set; }
        public List<DataUpdateResponse> Datas { get; set; }
    }
}
