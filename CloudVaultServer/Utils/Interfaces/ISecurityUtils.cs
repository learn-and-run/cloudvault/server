﻿using CloudVaultServer.DataAccess.EfModels;

namespace CloudVaultServer.Utils.Interfaces
{
    public interface ISecurityUtils
    {
        public User GetCurrentUser(string cookie);
    }
}
