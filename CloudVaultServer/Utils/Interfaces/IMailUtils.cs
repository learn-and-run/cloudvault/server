﻿namespace CloudVaultServer.Utils.Interfaces
{
    public interface IMailUtils
    {
        public bool SendMail(string email, string subject, string body);
    }
}
