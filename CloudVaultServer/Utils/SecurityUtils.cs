﻿using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.DataAccess.Repositories.Interfaces;
using CloudVaultServer.Utils.Interfaces;

namespace CloudVaultServer.Utils
{
    public class SecurityUtils : ISecurityUtils
    {
        private readonly IKnownConnectionRepository _knownConnectionRepository;
        private readonly IUserRepository _userRepository;

        public SecurityUtils(IKnownConnectionRepository knownConnectionRepository, IUserRepository userRepository)
        {
            _knownConnectionRepository = knownConnectionRepository;
            _userRepository = userRepository;
        }

        public User GetCurrentUser(string cookie)
        {
            if (cookie == null)
                return null;

            var knownConnection = _knownConnectionRepository.FindByCookie(cookie);
            return knownConnection == null ? null : _userRepository.FindById(knownConnection.UserId);
        }
    }
}
