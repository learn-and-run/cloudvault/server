﻿using CloudVaultServer.Utils.Interfaces;
using System;
using System.Net;
using System.Net.Mail;

namespace CloudVaultServer.Utils
{
    public class MailUtils : IMailUtils
    {
        public bool SendMail(string email, string subject, string body)
        {
            try
            {
                var client = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential("noreply.learnandrun@gmail.com", "4Yx}NMFxe!tD"),
                };

                var mailMessage = new MailMessage
                {
                    From = new MailAddress("noreply.learnandrun@gmail.com"),
                    Subject = subject,
                    Body = body
                };
                
                mailMessage.To.Add(email);
                client.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
    }
}
