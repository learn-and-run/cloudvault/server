﻿using CloudVaultServer.BusinessManagement.Interfaces;
using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.DataAccess.Repositories.Interfaces;
using CloudVaultServer.Dbo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CloudVaultServer.BusinessManagement
{
    /// <summary>
    /// Class DataManagement
    /// </summary>
    public class DataManagement : IDataManagement
    {
        private readonly IDataRepository _dataRepository;

        /// <summary>
        /// Constructor DataManagement
        /// </summary>
        /// <param name="dataRepository"></param>
        public DataManagement(IDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
        }

        /// <summary>
        /// Function which update the information of the user
        /// </summary>
        /// <param name="userId"> the id of the user</param>
        /// <param name="updateObjectBody">Object with information to update</param>
        /// <returns></returns>
        public UpdateObjectResponse UpdateUser(long userId, UpdateObjectBody updateObjectBody)
        {
            var res = new UpdateObjectResponse
            {
                LastSync = DateTime.Now,
                Datas = new List<DataUpdateResponse>()
            };

            updateObjectBody.Datas.ForEach(dataUpdate =>
            {
                var serverId = dataUpdate.ServerId;

                // create the data
                if (serverId == 0)
                {
                    res.Datas.Add(CreateData(userId, dataUpdate, res.LastSync));
                    return;
                }

                var data = _dataRepository.FindById(serverId);
                // the user try to break the application
                if (data == null || data.UserId != userId)
                    return;

                // data is not the latest
                if (data.LastUpdate > dataUpdate.LastUpdate)
                    return;

                // update the dates
                data.LastUpdate = dataUpdate.LastUpdate;
                data.LastSync = res.LastSync;

                var metaData = dataUpdate.MetaData;
                // delete the data
                if (metaData == null)
                {
                    RemoveDataReference(data);
                    return;
                }

                // update the data
                WriteDataInFile(dataUpdate.MetaData, data.MetaDataPath);
                WriteDataInFile(dataUpdate.Data, data.DataPath);
                _dataRepository.Update(data);
            });

            // get unknown datas
            res.Datas.AddRange(GetMissingUpdatedData(userId, updateObjectBody.LastSync, res.LastSync));

            return res;
        }

        /// <summary>
        /// Function which create data of the user
        /// </summary>
        /// <param name="userId">The id of the user</param>
        /// <param name="dataUpdate">Object with data update</param>
        /// <param name="dateTime">the date which data are created </param>
        /// <returns></returns>
        private DataUpdateResponse CreateData(long userId, DataUpdateBody dataUpdate, DateTime dateTime)
        {
            var newData = _dataRepository.Insert(new Data
            {
                UserId = userId,
                LastUpdate = dataUpdate.LastUpdate,
                LastSync = dateTime,
                MetaDataPath = WriteDataInFile(dataUpdate.MetaData),
                DataPath = WriteDataInFile(dataUpdate.Data)
            });

            return new DataUpdateResponse
            {
                ServerId = newData.Id,
                LocalGuid = dataUpdate.LocalGuid
            };
        }

        /// <summary>
        /// Function which return all data that the user does not have
        /// </summary>
        /// <param name="userId">the id of the user</param>
        /// <param name="from">the date of the latest synchronisation</param>
        /// <param name="to">the date of the new synchronisation</param>
        /// <returns>the data that the user does not have</returns>
        private IEnumerable<DataUpdateResponse> GetMissingUpdatedData(long userId, DateTime? from, DateTime to)
        {
            var missingUpdatedData = from != null ? _dataRepository.FindByUserBetween(userId, (DateTime) from, to) : _dataRepository.FindByUserUntil(userId, to);
            if (missingUpdatedData == null)
                return null;

            var res = new List<DataUpdateResponse>();

            missingUpdatedData.ForEach(data =>
            {
                string metaDataValue = null;
                string dataValue = null;
                try
                {
                    metaDataValue = ReadDataFromFile(data.MetaDataPath);
                    dataValue = ReadDataFromFile(data.DataPath);
                }
                catch (Exception)
                {
                    RemoveDataReference(data);
                }

                res.Add(new DataUpdateResponse
                {
                    ServerId = data.Id,
                    LastUpdate = data.LastUpdate,
                    MetaData = metaDataValue,
                    Data = dataValue
                });
            });

            return res;
        }

        /// <summary>
        /// Function which remove the reference of the data
        /// </summary>
        /// <param name="data">data which the reference will be removed</param>
        private void RemoveDataReference(Data data)
        {
            try
            {
                File.Delete(data.MetaDataPath);
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                File.Delete(data.DataPath);
            }
            catch (Exception)
            {
                // ignored
            }

            data.MetaDataPath = null;
            data.DataPath = null;
            _dataRepository.Update(data);
        }

        /// <summary>
        /// Function to read data stored in a file
        /// </summary>
        /// <param name="path">the path of the file were data are stored</param>
        /// <returns>Null if the path is not found or does not exist else the data </returns>
        private static string ReadDataFromFile(string path)
        {
            return path == null ? null : File.ReadAllText(path, Encoding.UTF8);
        }

        /// <summary>
        /// Function which write data into a file
        /// </summary>
        /// <param name="data">data to write in the file</param>
        /// <param name="path">the path of the file</param>
        /// <returns>return the path of the file created</returns>
        private static string WriteDataInFile(string data, string path = null)
        {
            path ??= CreateRandomFile();

            File.WriteAllTextAsync(path, data, Encoding.UTF8);

            return path;
        }

        /// <summary>
        /// Function which create a random file
        /// </summary>
        /// <returns>the path of the file</returns>
        private static string CreateRandomFile()
        {
            const string basePath = "datas/";
            if (!Directory.Exists(basePath))
                Directory.CreateDirectory(basePath);

            string guid;
            do
            {
                guid = Guid.NewGuid().ToString();
            } while (File.Exists(basePath + guid));

            File.Create(basePath + guid).Close();

            return basePath + guid;
        }
    }
}
