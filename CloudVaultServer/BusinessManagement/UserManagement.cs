﻿using CloudVaultServer.BusinessManagement.Interfaces;
using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.DataAccess.Repositories.Interfaces;
using CloudVaultServer.Dbo;
using CloudVaultServer.Utils.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace CloudVaultServer.BusinessManagement
{
    /// <summary>
    /// Class UserManagement
    /// </summary>
    public class UserManagement : IUserManagement
    {
        private readonly string _url = Environment.GetEnvironmentVariable("SERVER_ADDRESS");

        private readonly IUserRepository _userRepository;
        private readonly IActivationEmailRepository _activationEmailRepository;
        private readonly IValidationUserRepository _validationUserRepository;
        private readonly IKnownConnectionRepository _knownConnectionRepository;

        private readonly IMailUtils _mailUtils;

        /// <summary>
        /// Constructor UserManagement
        /// </summary>
        /// <param name="userRepository"></param>
        /// <param name="activationEmailRepository"></param>
        /// <param name="validationUserRepository"></param>
        /// <param name="knownConnectionRepository"></param>
        /// <param name="mailUtils"></param>
        public UserManagement(IUserRepository userRepository,
                                IActivationEmailRepository activationEmailRepository,
                                IValidationUserRepository validationUserRepository,
                                IKnownConnectionRepository knownConnectionRepository,
                                IMailUtils mailUtils)
        {
            _userRepository = userRepository;
            _activationEmailRepository = activationEmailRepository;
            _validationUserRepository = validationUserRepository;
            _knownConnectionRepository = knownConnectionRepository;
            _mailUtils = mailUtils;
        }

        /// <summary>
        /// Function which get the message of the user from his email
        /// </summary>
        /// <param name="email">the email of the user</param>
        /// <returns>Return the cipher message</returns>
        public Dictionary<string, string> GetMsg(string email)
        {
            var user = _userRepository.FindByEmail(email);
            if (user == null)
                return null;

            return new Dictionary<string, string>
            {
                { "cipherMsg", user.CipherMsg }
            };
        }

        /// <summary>
        /// Function which compare two list of bytes
        /// </summary>
        /// <param name="a">the first list of bytes</param>
        /// <param name="b">the second list of bytes</param>
        /// <returns>True if the two list are equals else false</returns>
        private static bool SlowEquals(IReadOnlyList<byte> a, IReadOnlyList<byte> b)
        {
            var diff = (uint)a.Count ^ (uint)b.Count;
            for (var i = 0; i < a.Count && i < b.Count; i++)
                diff |= (uint)(a[i] ^ b[i]);
            return diff == 0;
        }

        /// <summary>
        /// Function which log in the user
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public string Login(Connection connection)
        {
            var user = _userRepository.FindByEmail(connection.Email);
            if (user == null || !user.IsActive)
                return null;

            if (!SlowEquals(Encoding.UTF8.GetBytes(user.Msg), Encoding.UTF8.GetBytes(connection.Msg)))
                return null;

            string cookie;
            do
            {
                cookie = Guid.NewGuid().ToString();
            } while (!_knownConnectionRepository.IsCookieUnique(cookie));
            _knownConnectionRepository.Insert(new KnownConnection
            {
                Cookie = cookie,
                UserId = user.Id
            });

            return cookie;
        }

        public void Logout(string cookie)
        {
            var knownConnection = _knownConnectionRepository.FindByCookie(cookie);
            if (knownConnection != null)
                _knownConnectionRepository.Delete(knownConnection);
        }

        private static string GenerateRandomMsg()
        {
            var randomBytes = new byte[256];
            using var rngCsp = new RNGCryptoServiceProvider();
            rngCsp.GetBytes(randomBytes);
            return Convert.ToBase64String(randomBytes);
        }

        public NewMsg CreateUser(string email)
        {
            if (!_userRepository.IsEmailAvailable(email))
                return null;

            var user = new User
            {
                Msg = GenerateRandomMsg(),
                IsActive = false
            };
            _userRepository.Insert(user);

            string guid;
            do
            {
                guid = Guid.NewGuid().ToString();
            } while (!_activationEmailRepository.IsGuidUnique(guid));
            _activationEmailRepository.Insert(new ActivationEmail
            {
                Guid = guid,
                Email = email,
                UserId = user.Id
            });

            do
            {
                guid = Guid.NewGuid().ToString();
            } while (!_validationUserRepository.IsGuidUnique(guid));
            _validationUserRepository.Insert(new ValidationUser
            {
                Guid = guid,
                UserId = user.Id
            });

            return new NewMsg
            {
                Guid = guid,
                Msg = user.Msg
            };
        }

        public bool FinalizeUser(NewCipherMsg newCipherMsg)
        {
            var validationUser = _validationUserRepository.FindByGuid(newCipherMsg.Guid);
            if (validationUser == null)
                return false;
            _validationUserRepository.Delete(validationUser);

            var user = _userRepository.FindById(validationUser.UserId);
            user.CipherMsg = newCipherMsg.CipherMsg;
            _userRepository.Update(user);

            var activationEmail = _activationEmailRepository.FindByUserId(user.Id);
            if (activationEmail == null)
            {
                _userRepository.Delete(user);
                return false;
            }

            if (!_mailUtils.SendMail(activationEmail.Email, "Valider votre email", $"{_url}/Home/Activate/{activationEmail.Guid}"))
            {
                _activationEmailRepository.Delete(activationEmail);
                _userRepository.Delete(user);
                return false;
            }

            return true;
        }

        public bool ActivateUser(string id)
        {
            var activationEmail = _activationEmailRepository.FindByGuid(id);
            if (activationEmail == null)
                return false;

            var user = _userRepository.FindById(activationEmail.UserId);
            if (user.IsActive || user.CipherMsg == null)
                return false;

            var tmpEmail = activationEmail.Email;
            _activationEmailRepository.Delete(activationEmail);

            if (!_userRepository.IsEmailAvailable(tmpEmail))
                return false;

            user.IsActive = true;
            user.Email = tmpEmail;
            _userRepository.Update(user);

            _mailUtils.SendMail(user.Email, "Welcome", "Bienvenue sur CloudVault !");

            return true;
        }

        public bool UpdateEmail(User user, string email)
        {
            if (user.Email.Equals(email))
                return true;

            if (!_userRepository.IsEmailAvailable(email))
                return false;

            _activationEmailRepository.DeleteByUserId(user.Id);

            string guid;
            do
            {
                guid = Guid.NewGuid().ToString();
            } while (!_activationEmailRepository.IsGuidUnique(guid));

            var activationEmail = new ActivationEmail
            {
                Guid = guid,
                Email = email,
                UserId = user.Id
            };
            _activationEmailRepository.Insert(activationEmail);

            if (!_mailUtils.SendMail(email, "Tentative de mis à jour d'email", $"{_url}/Home/ValidateEmail/{guid}"))
            {
                _activationEmailRepository.Delete(activationEmail);
                return false;
            }

            return true;
        }

        public bool ValidateEmail(string id)
        {
            var activationEmail = _activationEmailRepository.FindByGuid(id);
            if (activationEmail == null)
                return false;

            var user = _userRepository.FindById(activationEmail.UserId);
            if (!user.IsActive)
                return false;

            var tmpEmail = activationEmail.Email;
            _activationEmailRepository.Delete(activationEmail);

            if (!_userRepository.IsEmailAvailable(tmpEmail))
                return false;

            user.Email = tmpEmail;
            _userRepository.Update(user);

            _mailUtils.SendMail(tmpEmail, "Changement d'email", "Votre email a bien été mis à jour !");

            return true;
        }
    }
}
