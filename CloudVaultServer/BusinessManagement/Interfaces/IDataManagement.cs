﻿using CloudVaultServer.Dbo;

namespace CloudVaultServer.BusinessManagement.Interfaces
{
    public interface IDataManagement
    {
        public UpdateObjectResponse UpdateUser(long userId, UpdateObjectBody updateObjectBody);
    }
}
