﻿using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.Dbo;
using System.Collections.Generic;

namespace CloudVaultServer.BusinessManagement.Interfaces
{
    public interface IUserManagement
    {
        public Dictionary<string, string> GetMsg(string email);
        public string Login(Connection connection);
        public void Logout(string cookie);
        public NewMsg CreateUser(string email);
        public bool FinalizeUser(NewCipherMsg newCipherMsg);
        public bool ActivateUser(string id);
        public bool UpdateEmail(User user, string email);
        public bool ValidateEmail(string id);
    }
}
