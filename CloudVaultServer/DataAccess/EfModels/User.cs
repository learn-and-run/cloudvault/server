﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudVaultServer.DataAccess.EfModels
{
    [Table("User", Schema = "dbo")]
    public class User
    {
        [Key]
        public long Id { get; set; }
        [StringLength(320)]
        public string Email { get; set; }
        [Required]
        public string Msg { get; set; }
        public string CipherMsg { get; set; }
        [Required]
        public bool IsActive { get; set; }
    }
}
