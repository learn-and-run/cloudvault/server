﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudVaultServer.DataAccess.EfModels
{
    [Table("ValidationUser", Schema = "dbo")]
    public class ValidationUser
    {
        [Key]
        [StringLength(36)]
        public string Guid { get; set; }
        [Required]
        public long UserId { get; set; }
    }
}
