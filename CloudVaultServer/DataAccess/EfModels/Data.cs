﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudVaultServer.DataAccess.EfModels
{
    [Table("Data", Schema = "dbo")]
    public class Data
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public long UserId { get; set; }
        [StringLength(256)]
        public string MetaDataPath { get; set; }
        [StringLength(256)]
        public string DataPath { get; set; }
        [Required]
        public DateTime LastUpdate { get; set; }
        [Required]
        public DateTime LastSync { get; set; }
    }
}
