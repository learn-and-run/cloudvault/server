﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudVaultServer.DataAccess.EfModels
{
    [Table("KnownConnection", Schema = "dbo")]
    public class KnownConnection
    {
        [Required]
        public long UserId { get; set; }
        [Key]
        [StringLength(36)]
        public string Cookie { get; set; }
    }
}
