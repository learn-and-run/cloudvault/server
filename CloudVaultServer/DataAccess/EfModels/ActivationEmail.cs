﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudVaultServer.DataAccess.EfModels
{
    [Table("ActivationEmail", Schema = "dbo")]
    public class ActivationEmail
    {
        [Key]
        [StringLength(36)]
        public string Guid { get; set; }
        [Required]
        public long UserId { get; set; }
        [Required]
        public string Email { get; set; }
    }
}
