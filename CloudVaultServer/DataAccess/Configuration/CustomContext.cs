﻿using CloudVaultServer.DataAccess.EfModels;
using Microsoft.EntityFrameworkCore;

namespace CloudVaultServer.DataAccess.Configuration
{
    public class CustomContext : DbContext
    {
        public CustomContext()
        {}
        public CustomContext(DbContextOptions<CustomContext> options) : base(options)
        {}

        public DbSet<ActivationEmail> ActivationEmail { get; set; }
        public DbSet<Data> Data { get; set; }
        public DbSet<KnownConnection> KnownConnection { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<ValidationUser> ValidationUser { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ActivationEmail>(entity =>
            {
                entity.HasIndex(e => new { e.UserId, e.Email }).IsUnique();
            });

            builder.Entity<Data>(entity =>
            {
                entity.HasIndex(e => new { e.MetaDataPath, e.DataPath }).IsUnique();
            });

            builder.Entity<ValidationUser>(entity =>
            {
                entity.HasIndex(e => e.UserId).IsUnique();
            });
        }
    }
}
