﻿using AutoMapper;
using CloudVaultServer.DataAccess.Configuration;
using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.DataAccess.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace CloudVaultServer.DataAccess.Repositories
{
    public class ActivationEmailRepository : Repository<ActivationEmail>, IActivationEmailRepository
    {
        public ActivationEmailRepository(CustomContext context, ILogger<ActivationEmailRepository> logger, IMapper mapper) : base(context, logger, mapper)
        {}

        public ActivationEmail FindByGuid(string guid)
        {
            return _context.ActivationEmail.FirstOrDefault(entity => entity.Guid.Equals(guid));
        }

        public bool IsGuidUnique(string guid)
        {
            return _context.ActivationEmail.FirstOrDefault(entity => entity.Guid.Equals(guid)) == null;
        }

        public ActivationEmail FindByUserId(long id)
        {
            return _context.ActivationEmail.FirstOrDefault(entity => entity.UserId == id);
        }

        public void DeleteByUserId(long id)
        {
            var activationEmail = _context.ActivationEmail.FirstOrDefault(entity => entity.UserId == id);
            if (activationEmail != null)
            {
                _context.ActivationEmail.Remove(activationEmail);
            }
        }
    }
}
