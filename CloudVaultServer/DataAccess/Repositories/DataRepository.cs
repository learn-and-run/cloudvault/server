﻿using AutoMapper;
using CloudVaultServer.DataAccess.Configuration;
using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.DataAccess.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CloudVaultServer.DataAccess.Repositories
{
    public class DataRepository : Repository<Data>, IDataRepository
    {
        public DataRepository(CustomContext context, ILogger<DataRepository> logger, IMapper mapper) : base(context, logger, mapper)
        {}

        public Data FindById(long id)
        {
            return _context.Data.FirstOrDefault(entity => entity.Id == id);
        }

        public List<Data> FindByUserBetween(long userId, DateTime from, DateTime to)
        {
            return _context.Data.Where(entity => entity.UserId == userId && DateTime.Compare(from, entity.LastSync) < 0 && DateTime.Compare(entity.LastSync, to) < 0).ToList();
        }

        public List<Data> FindByUserUntil(long userId, DateTime to)
        {
            return _context.Data.Where(entity => entity.UserId == userId && DateTime.Compare(entity.LastSync, to) < 0).ToList();
        }
    }
}
