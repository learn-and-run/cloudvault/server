﻿using AutoMapper;
using CloudVaultServer.DataAccess.Configuration;
using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.DataAccess.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace CloudVaultServer.DataAccess.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(CustomContext context, ILogger<UserRepository> logger, IMapper mapper) : base(context, logger, mapper)
        {}

        public User FindById(long id)
        {
            return _context.User.FirstOrDefault(x => x.Id == id);
        }

        public bool IsEmailAvailable(string email)
        {
            return _context.User.FirstOrDefault(user => email.Equals(user.Email)) == null;
        }

        public User FindByEmail(string email)
        {
            return _context.User.FirstOrDefault(user => email.Equals(user.Email));
        }
    }
}
