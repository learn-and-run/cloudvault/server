﻿using AutoMapper;
using CloudVaultServer.DataAccess.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using CloudVaultServer.DataAccess.Repositories.Interfaces;

namespace CloudVaultServer.DataAccess.Repositories
{
    public class Repository<TDbEntity> : IRepository<TDbEntity>
        where TDbEntity : class, new()
    {
        private readonly DbSet<TDbEntity> _set;
        protected readonly CustomContext _context;
        protected ILogger _logger;
        protected IMapper _mapper;

        protected Repository(CustomContext context, ILogger logger, IMapper mapper)
        {
            _context = context;
            _logger = logger;
            _mapper = mapper;
            _set = _context.Set<TDbEntity>();
        }

        public virtual IEnumerable<TDbEntity> Get(string includeTables = "")
        {
            try
            {
                var query = string.IsNullOrEmpty(includeTables) ? _set.AsNoTracking().ToList() : _set.Include(includeTables).AsNoTracking().ToList();

                return _mapper.Map<TDbEntity[]>(query);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error on DB", ex);
                return null;
            }
        }

        public virtual TDbEntity Insert(TDbEntity entity)
        {
            _set.Add(entity);
            try
            {
                _context.SaveChanges();
                Console.WriteLine(entity);
                return entity;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error on DB", ex);
                return null;
            }
        }

        public virtual TDbEntity Update(TDbEntity entity)
        {
            var newEntity = _set.Update(entity);
            try
            {
                _context.SaveChanges();
                return newEntity.Entity;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error on DB", ex);
                return null;
            }
        }

        public virtual void Delete(TDbEntity entity)
        {
            _set.Remove(entity);
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error on DB", ex);
            }
        }
    }
}
