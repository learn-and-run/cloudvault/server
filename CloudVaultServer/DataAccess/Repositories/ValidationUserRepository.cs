﻿using AutoMapper;
using CloudVaultServer.DataAccess.Configuration;
using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.DataAccess.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace CloudVaultServer.DataAccess.Repositories
{
    public class ValidationUserRepository : Repository<ValidationUser>, IValidationUserRepository
    {
        public ValidationUserRepository(CustomContext context, ILogger<ValidationUserRepository> logger, IMapper mapper) : base(context, logger, mapper)
        {}

        public bool IsGuidUnique(string guid)
        {
            return _context.ValidationUser.FirstOrDefault(entity => entity.Guid.Equals(guid)) == null;
        }

        public ValidationUser FindByGuid(string guid)
        {
            return _context.ValidationUser.FirstOrDefault(entity => entity.Guid.Equals(guid));
        }
    }
}
