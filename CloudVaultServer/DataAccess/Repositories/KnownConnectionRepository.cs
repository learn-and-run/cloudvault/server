﻿using AutoMapper;
using CloudVaultServer.DataAccess.Configuration;
using CloudVaultServer.DataAccess.EfModels;
using CloudVaultServer.DataAccess.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace CloudVaultServer.DataAccess.Repositories
{
    public class KnownConnectionRepository : Repository<KnownConnection>, IKnownConnectionRepository
    {
        public KnownConnectionRepository(CustomContext context, ILogger<KnownConnectionRepository> logger, IMapper mapper) : base(context, logger, mapper)
        {}

        public bool IsCookieUnique(string cookie)
        {
            return _context.KnownConnection.FirstOrDefault(entity => entity.Cookie.Equals(cookie)) == null;
        }

        public KnownConnection FindByCookie(string cookie)
        {
            return cookie == null ? null : _context.KnownConnection.FirstOrDefault(entity => entity.Cookie.Equals(cookie));
        }
    }
}
