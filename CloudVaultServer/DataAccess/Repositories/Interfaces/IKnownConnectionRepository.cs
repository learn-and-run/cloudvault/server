﻿using CloudVaultServer.DataAccess.EfModels;

namespace CloudVaultServer.DataAccess.Repositories.Interfaces
{
    public interface IKnownConnectionRepository : IRepository<KnownConnection>
    {
        public bool IsCookieUnique(string cookie);
        public KnownConnection FindByCookie(string cookie);
    }
}
