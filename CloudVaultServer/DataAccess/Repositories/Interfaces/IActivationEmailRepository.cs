﻿using CloudVaultServer.DataAccess.EfModels;

namespace CloudVaultServer.DataAccess.Repositories.Interfaces
{
    public interface IActivationEmailRepository : IRepository<ActivationEmail>
    {
        public ActivationEmail FindByGuid(string guid);
        public bool IsGuidUnique(string guid);
        public ActivationEmail FindByUserId(long id);
        public void DeleteByUserId(long id);
    }
}
