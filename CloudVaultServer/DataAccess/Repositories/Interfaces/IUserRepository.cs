﻿using CloudVaultServer.DataAccess.EfModels;

namespace CloudVaultServer.DataAccess.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        public User FindById(long id);
        public bool IsEmailAvailable(string email);
        public User FindByEmail(string email);
    }
}
