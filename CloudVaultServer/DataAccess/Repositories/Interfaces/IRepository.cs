﻿using System.Collections.Generic;

namespace CloudVaultServer.DataAccess.Repositories.Interfaces
{
    public interface IRepository<TDbEntity>
    {
        public IEnumerable<TDbEntity> Get(string includeTables = "");
        public TDbEntity Insert(TDbEntity entity);
        public TDbEntity Update(TDbEntity entity);
        public void Delete(TDbEntity entity);
    }
}
