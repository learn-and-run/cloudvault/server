﻿using CloudVaultServer.DataAccess.EfModels;

namespace CloudVaultServer.DataAccess.Repositories.Interfaces
{
    public interface IValidationUserRepository : IRepository<ValidationUser>
    {
        public bool IsGuidUnique(string guid);
        public ValidationUser FindByGuid(string guid);
    }
}
