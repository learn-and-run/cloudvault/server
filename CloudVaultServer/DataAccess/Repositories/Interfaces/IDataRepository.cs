﻿using CloudVaultServer.DataAccess.EfModels;
using System;
using System.Collections.Generic;

namespace CloudVaultServer.DataAccess.Repositories.Interfaces
{
    public interface IDataRepository : IRepository<Data>
    {
        public Data FindById(long id);
        public List<Data> FindByUserBetween(long userId, DateTime from, DateTime to);
        public List<Data> FindByUserUntil(long userId, DateTime to);
    }
}
