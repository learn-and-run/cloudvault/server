FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY . .
RUN dotnet restore   
RUN dotnet build  -c Release -o /app/build

FROM build AS publish
RUN dotnet publish -c Release -o /app/publish

FROM publish AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY ./CloudVaultServer/CloudVaultServer.xml .
CMD ASPNETCORE_URLS=http://*:8080 dotnet CloudVaultServer.dll