# CloudVault Server

## Presentation

CloudVault is a secure data manager. You can store securely passwords, notes, and credit cards. 
Your data is encrypted using strong algorithms for encryption so that only you can read it. 
All of this process is based on your master password you give when you create an account.

Your data is stored locally and can also be send on a remote server to permit synchronization between your multiple devices.

## Architecture

We are using docker for our solution to facilitate the deployement on all devices.

The Dockefile located at the root of the project is used to build the back-end solution in a container.

The docker-compose.yaml, also at the root of the project, is used to start the entire application. 
It starts a Nginx instance, a back-end instance, and a SQL Server instance.

### Docker compose

Nginx will listen for requests and transfer those to the back-end through a docker network. 
Then the back-end will handle the request to give a response. 
An other docker network permits the back-end to interact with the database.

## Deployement

To deploy this solution on your machine, you need to install `docker` : https://www.docker.com/ 
Once this is done, you can execute the following command to start the containers : `docker-compose up --build`. 
It will download the different images you need and then start the server.

**WARNING**: the *docker-compose.yaml* file is configure for our server. 
If you intent to launch the server on your personal computer or your remote server, please change the server address to your ip address or your domain name in the *docker-compose.yaml* file (row 33). 
To make it works on your local machine, you can use the IP of your docker machine. To start your docker machine, use the following command `docker-machine start`, and to get its IP do `docker-machine ip`. 
The Nginx container will listen by default on port `8080`. You can change it if it is already taken by modifying the *docker-compose.yaml* (row 51).

Also, the docker containing the database will need at least 2000 Megabytes of memory, so be sure the docker machine has enough to run.